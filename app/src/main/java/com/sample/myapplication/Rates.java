package com.sample.myapplication;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

class Rates {

  @SerializedName("AUD")
  @Expose
  public Double AUD;
  @SerializedName("CAD")
  @Expose
  public Double CAD;
  @SerializedName("CHF")
  @Expose
  public Double CHF;
  @SerializedName("CYP")
  @Expose
  public Double CYP;
  @SerializedName("CZK")
  @Expose
  public Double CZK;
  @SerializedName("DKK")
  @Expose
  public Double DKK;
  @SerializedName("EEK")
  @Expose
  public Double EEK;
  @SerializedName("GBP")
  @Expose
  public Double GBP;
  @SerializedName("HKD")
  @Expose
  public Double HKD;
  @SerializedName("HUF")
  @Expose
  public Double HUF;
  @SerializedName("ISK")
  @Expose
  public Double ISK;
  @SerializedName("JPY")
  @Expose
  public Double JPY;
  @SerializedName("KRW")
  @Expose
  public Double KRW;
  @SerializedName("LTL")
  @Expose
  public Double LTL;
  @SerializedName("LVL")
  @Expose
  public Double LVL;
  @SerializedName("MTL")
  @Expose
  public Double MTL;
  @SerializedName("NOK")
  @Expose
  public Double NOK;
  @SerializedName("NZD")
  @Expose
  public Double NZD;
  @SerializedName("PLN")
  @Expose
  public Double PLN;
  @SerializedName("ROL")
  @Expose
  public Integer ROL;
  @SerializedName("SEK")
  @Expose
  public Double SEK;
  @SerializedName("SGD")
  @Expose
  public Double SGD;
  @SerializedName("SIT")
  @Expose
  public Double SIT;
  @SerializedName("SKK")
  @Expose
  public Double SKK;
  @SerializedName("TRL")
  @Expose
  public Integer TRL;
  @SerializedName("USD")
  @Expose
  public Double USD;
  @SerializedName("ZAR")
  @Expose
  public Double ZAR;

  // this is just so that we can use .toString() to turn this object into a string so that we can display it
  @Override
  public String toString() {
    return "Rates{" +
        "AUD=" + AUD +
        ", CAD=" + CAD +
        ", CHF=" + CHF +
        ", CYP=" + CYP +
        ", CZK=" + CZK +
        ", DKK=" + DKK +
        ", EEK=" + EEK +
        ", GBP=" + GBP +
        ", HKD=" + HKD +
        ", HUF=" + HUF +
        ", ISK=" + ISK +
        ", JPY=" + JPY +
        ", KRW=" + KRW +
        ", LTL=" + LTL +
        ", LVL=" + LVL +
        ", MTL=" + MTL +
        ", NOK=" + NOK +
        ", NZD=" + NZD +
        ", PLN=" + PLN +
        ", ROL=" + ROL +
        ", SEK=" + SEK +
        ", SGD=" + SGD +
        ", SIT=" + SIT +
        ", SKK=" + SKK +
        ", TRL=" + TRL +
        ", USD=" + USD +
        ", ZAR=" + ZAR +
        '}';
  }
}