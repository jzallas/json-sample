package com.sample.myapplication;

import retrofit.Call;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;
import retrofit.http.GET;

public class CurrencyConverter {

  private FixerIoService fixerIo;

  public CurrencyConverter() {

    // Create a retrofit instance - this is the magical library that gets the JSON data from the internet and turns it into an object for you
    Retrofit retrofit = new Retrofit.Builder()
        .baseUrl("https://api.fixer.io")
        .addConverterFactory(GsonConverterFactory.create())
        .build();

    fixerIo = retrofit.create(FixerIoService.class);
  }

  public Call<CurrencyData> getLatest(){
    return fixerIo.getLatest();
  }

  interface FixerIoService {

    @GET("/latest")
    Call<CurrencyData> getLatest();
  }

}
