package com.sample.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import java.io.IOException;

import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class MainActivity extends AppCompatActivity {

  private TextView sampleTextView;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    sampleTextView = (TextView) findViewById(R.id.sampleTextView);

    // create a converter object - this connects to the internet and gets a currency data object used to do conversions
    CurrencyConverter converter = new CurrencyConverter();

    converter.getLatest() // get the latest CurrencyData
        .enqueue( // this request happens on the background so we need to put the response in a queue
            new Callback<CurrencyData>() {
              @Override
              public void onResponse(Response<CurrencyData> response, Retrofit retrofit) {
                // when successful, we use the data here
                CurrencyData data = response.body();
                sampleTextView.setText(data.toString());
              }

              @Override
              public void onFailure(Throwable t) {
                // if something messed up, display the message
                sampleTextView.setText("Something went wrong");
              }
            });


  }
}
